Welcome to Gront lab laboratory protocols
===========================================================

This repository provides notes, protocols and tutorials related to multiscale
modeling of biomacromolecules, primarily proteins and their complexes, that are
used in daily research tasks by Gront lab (http://bioshell.pl). While documentation
related to BioShell package (bioinformatics and biosimulation tools) can be found
on a separate website: http://bioshell.readthedocs.io/, these pages provide practical solutions to common calculations and modelling scenarios.

We make these protocols available under `Apache 2.0 license`_.

BioShell protocols
------------------

.. toctree::
   :maxdepth: 2
   :caption: BioShell protocols
   :name: BioShell
   :hidden:

   doc/ligand_clustering.rst   
   
Protocols that utilize mainly BioShell applications:

   - :ref:`ligand_clustering`

Rosetta protocols
------------------

.. toctree::
   :maxdepth: 2
   :caption: Rosetta protocols
   :name: Rosetta
   :hidden:

   doc/preliminaries.rst   
   doc/fragment_picking.rst   
   doc/ab-initio.rst
   doc/score_patch.rst
   doc/rosetta_p450_docking.rst

These pages provide information about Rosetta protocols used by the Gront lab,  including Rosetta flag-files and handy one liners that can be copied & pasted into your terminal. To see the full documentaion however, visit rosettacommons.org website

   - :ref:`preliminaries`
   - :ref:`doc_fragment_picking`
   - :ref:`doc_ab-initio`
   - :ref:`score_patch`
   - :ref:`pyrosetta_cyp51_docking`
   
Modeller protocols
------------------

.. toctree::
   :maxdepth: 1
   :caption: Modeller protocols
   :name: Modeller
   :hidden:

   doc/comparative_modelling_modeller.rst   

Modeller is a widely recognized tool for homology modelling of protein structures.

  - :ref:`comparative_modelling_modeller`
   
PyMOL notes
------------------

.. toctree::
   :maxdepth: 1
   :caption: notes on PyMOL
   :name: pymol
   :hidden:

   doc/pymol.rst   

Modeller is a widely recognized tool for homology modelling of protein structures.

  - :ref:`comparative_modelling_modeller`

Bioinformatic tools
--------------------

.. toctree::
   :maxdepth: 1
   :caption: Bioinformatic tools
   :name: Bioinformatics
   :hidden:

   doc/bioinfo_tools.rst   

This section desribes common bioinformatics tools such as psiblast or HH-suite

  - :ref:`bioinfo_tools`


Notable links related to Rosetta
--------------------------------

Rosetta testing server: http://benchmark.graylab.jhu.edu/
PyRosetta tutorials: http://www.pyrosetta.org/tutorials

.. toctree::
   :maxdepth: 1
   :caption: Other notes
   :name: other

   doc/git-notes
   doc/jobs_on_cluster

Other links
-----------

 - RestructuredText format guide: https://documentation-style-guide-sphinx.readthedocs.io/
 - Sphinx documentation: http://www.sphinx-doc.org/en/master/

.. rubric::
  References

.. target-notes::

.. _`Apache 2.0 license` : https://www.apache.org/licenses/LICENSE-2.0
