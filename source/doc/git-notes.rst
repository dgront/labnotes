.. _git_notes:

Notes on ``git``
====================================

BioShell code is versioned by  ``git``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To checkout the newest source, run the following:
    ``git clone git@bitbucket.org:dgront/bioshell.git``

This will create a local directory named ``bioshell`` containing the source. Note, that you need bitbucket account to do this. Also, SSH key of the local machine must be registered at bitbucket.


Working in branches
~~~~~~~~~~~~~~~~~~~


  -  **Creating a local branch**

    ``git checkout -b name_of_branch``
    The new branch will hold all the changes you've intoduced to the branch you diverged from (which most likely is ``origin/master``)

  -  **Uploading a local branch**

    To push a local branch to bitbucket **for the very first time**, do the following:
    ``git push -u origin name_of_branch``
    
  -  **Merging changes from** *master* **branch to a current branch** *feature*

    | ``git checkout feature``    
    | ``git merge master``
  
  - **Cleaning up old branches**

    | First chech which branches are already merged in *master* and can be easily removed:
    | ``git checkout master``
    | ``git branch --merged``
    | Then remove a merged branch locally and on the remote:
    | ``git branch -d obsolete-branch``
    | ``git push origin --delete obsolete-branch``

Moving around the repository
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  -  **Check history of a particular subdirectory (10 last commits)**

    ``git log --name-status -10 path/to/dir``


  - **Looking up a file that has been deleted in the past**

    ``git log --all --full-history -- **/Simulation*Settings.cc``

    where ``SimulationSettings.cc`` is the actually deleted file; If you are not sure about spelling the name of the file, use '*'
    as in this example