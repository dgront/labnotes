.. _preliminaries:

Preliminary information and input/output file processing 
==========================================================

Using Rosetta applications
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Rosetta programs (executables) re located in your `main/source/bin` directory. Each application name has an extendion, denoting
compilation platform and variant. For example, programs compiled on mac with clang the extension is `.macosclangrelease`, while
compiled on Linux with `gcc` - `.linuxgccrelease`

Idealising input PDB file for Rosetta
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following command reads in a PDB file and writes that protein in an ideal form, i.e. with all
bond lenghts and planar angles according to the Rosetta definition. Next step - run "idealize_jd2" program as follows:
    
.. code-block:: console

   idealize_jd2.macosclangrelease -in:file:s 1tenA.pdb 
      
      

Extract PDBs from an output silent file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Silent file (``.out`` file) produced by Rosetta may be converted into a set of separate PDB files with ``extract_pdbs`` program:

.. code-block:: console

  extract_pdbs.linuxgccrelease -in:file:silent default.out -out:prefix ./out_pdb/
  
where ``out_pdb`` is the name of output directory *which must exist* (Rosetta does not create it on her own)

To extract only selected frames, use: ``-in::file:tags``:

.. code-block:: console

  extract_pdbs.linuxgccrelease -in:file:silent default.out -out:prefix ./out_pdb/ -in::file:tags S_00001234 S_00000123
  

