.. _pymol:


Loading input files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 - ``fetch 2gb1``: loads a pdb/cif file
 - ``set assembly, 1``; ``fetch 6bb5, 6bb5_full`` loads a `biological unit`_ for the *6bb5* as *6bb5_full*
 

Selecting stuff
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 - ``select resn hem`` selects by residue name
 - ``select resi 12+13+14`` selects three residues by residue index
 - ``select bb, name C+N+CA+O`` selects protein backbone atoms by  name, selection is stored under the name *bb* (as backbone)
 - ``select sidechains_A, chain A and (not bb)`` selects sidechains from chain A just by selecting anything that is not backbone
 - ``select segment, resi 155-167`` selects three residues and names them *segment*
 - ``select !polymer`` selects all water molecules, bufefer, cofactors etc

See also `selection algebra`_  page on PyMOL wiki
 
Hide & Show
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 - ``show surface`` shows surface for the whole object
 - ``hide cartoon`` hides all cartoons
 - ``show sticks, segment`` show the object *“segment”* as sticks
 - ``set seq_view, on`` shows the sequence
 - to display a CA trace with sticks connecting:
     ```
    set ribbon_trace,1
    set ribbon_sampling,1
    show ribbon
     ```
 
Extract, copy or remove
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 - ``remove solvent``: remove solvent (water molecules)
 - ``remove resn hoh``: remove residues by name - in this case also removes water molecules

Working with electron density
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 - ``fetch 3av2, type=2fofc`` download the 2fofc electron density map; **Note**, that the density will be loaded into an object named `3av2_2fofc`
 - ``map_double 3av2_2fofc`` doubles the grid for  electron density map tabulation
 - ``isomesh 3av2_mesh, 3av2_2fofc, 2.0`` create a mesh object named ``3av2_mesh`` for the whole cell, using the density map 3av2_2fofc; use sigma=2.0 when displaying
 - ``isomesh map, 3av2_2fofc, 2.0, 3av2`` draws a mesh just for the *3av2* object
 

Miscellanea
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

 - ``reinit``: clear and delete everything
 - ``get_symmetry object-name``: prints unit cell parameters and space group name as e.g. "P212121"

.. rubric::
  References

.. target-notes::

.. _`biological unit` : https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/biological-assemblies
.. _`selection algebra` : https://pymolwiki.org/index.php/Selection_Algebra