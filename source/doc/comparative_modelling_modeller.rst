.. _comparative_modelling_modeller:

Homology Modelling that includes ligands
================================================================

Modeller is a widely known tool for Homology Modelling. Elaborate documentation can be found on `this site <https://salilab.org/modeller/tutorial/>`_. In brief, you need three files to run Modeller:
  
  - template structure (or structures) in PDB format.
    This file must have `.pdb`, `.ent` or `.atm` extension

  - target to template alignment in PIR format

  - modelling script - Python `.py` file, say `my_modeller_script.py` (name of that file is not important)

Template selectionand target-to-template alignment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A convenient possibility to obtain target to template alignment is to use  `HHPred web server <https://toolkit.tuebingen.mpg.de/tools/hhpred>`_, which searches for the best templates and provides the respective alignment in the PIR format. Moreover, you can run modeller directly on the HHPred website. The server however allows only for a very simple usage. 

Here we used a local version of modeller program that allowed us to take into account a cofactor molecule (heme) and a ligand during the model building process. This requires manual editing a `.pir` file. An example alignment file is given below:


    .. code-block:: bash
        
		>P1;00040
		sequence:00040:1    :A:555  :A::::
		MSAIIPQVQQLLGQVAQFIPPWFAALPTSVKVVIAVIGIPALVICLNVFQQLCLPRRKDLPPVVFHYIPWFGSAAYYGEDPYKFLF
		ECRDKYGDLFTFILMGRRVTVALGPKGNNLSLGGKISQVSAEEAYTHLTTPVFGKGVVYDCPNEMLMQQKKFIKSGLTTESLQSYP
		PMITSECEDFFTKEVGISP-QKPSATLDLLKSMSELIILTASRTLQGKEVRESLNGQFAKYYEDLDGGFTPLNFMFPNLPLPSYKR
		RDEAQKAMSDFYLKIMENRRKGESDHEHDMIENLQSCKYRNG-VPLSDRDIAHIMIALLMAGQHTSSATSSWTLLHLADRPDVVEA
		LYQEQKQKLGNPDGTFRDYKYEDLKELPIMDSIIRETLRMHAPIHSIYRKVLSDIPVPPSLSAPSENGQ--YIIPKGHYIMAAPGV
		SQMDPRIWQDAKVWNPARWHDEKGFAAAAMAQYSKAEQVDYGFGSVSKGTESPYQPFGAGRHRCVGEQFAYTQLSTIFTYVVRNFT
		LKLA--VPKFPETNYRTMIVQPNNPLVTFTLRNAEVK/..*
		>P1;4LXJ
		structure:4LXJ: :A: :A::Saccharomyces cerevisiae:1.9:
		-----SIVGEALEYVNIGLSHFLAL----PLAQRISLIIIIPFIYNIVWQLLYSLRKDRPPLVFYW-IPWVGSAVVYGMKPYEFFE
		ECQKKYGDIFSFVLLGRVMTVYLGPKGHEFVFNAKLADVSAEAAYAHLTTPVFGKGVIYDCPNSRLMEQKKFVKGALTKEAFKSYV
		PLIAEEVYKYFRDSKNFRLNERTTGTIDVMVTQPEMTIFTASRSLLGKEMRAKLDTDFAYLYSDLDKGFTPINFVFPNLPLEHYRK
		RDHAQKAISGTYMSLIKERRKNNDIQDRDLIDSLMKNSTYKDGVKMTDQEIANLLIGVLMGGQHTSAATSAWILLHLAERPDVQQE
		LYEEQMRVLDGGK---KELTYDLLQEMPLLNQTIKETLRMHHPLHSLFRKVMKDMHV----------PNTSYVIPAGYHVLVSPGY
		THLRDEYFPNAHQFNIHRWNNDSA------SSYSVGEEVDYGFGAISKGVSSPYLPFGGGRHRCIGEHFAYCQLGVLMSIFIRTLK
		WHYPEGKTVPPPDFTSMVTLPTGPAKIIWEKRNPEQK/..*


Note, that:

  - There must be exactly one `sequence` entry in the `.pir` file as well as at least one `structure` entry. 
  - The header of the `sequence` entry provides residue range for that sequence (residues from 1 to 555). 
  - In the case of `structure`, the sequence range is not specified explicitely. The respective fields are left blank and Modeller will guess them from the PDB coordinates.
  - Unlike the FASTA format, sequences ends with '*' character
  - The `/..` character at the very end of each sequence denote a chain break (`/` character) and two ligand residues (each of them represented as '.'). The template structure must contain exactly two small molecules, i.e. haem and ligand (Lanosterol in this case). All other residues (e.g. SO4, HOH, etc.) should be removed from each tempalate PDB file.


Modelling script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The last file is the Python script that runs the modelling calculations. You can adapt a script found in the Modelling tutorial. Here we use the following:
    
    .. code-block:: Python
        
		# Comparative modeling by the automodel class
		from modeller import *              # Load standard Modeller classes
		from modeller.automodel import *    # Load the automodel class

		log.verbose()    # request verbose output
		env = environ()  # create a new MODELLER environment to build this model in
		env.io.hetatm = True

		# directories for input atom files
		env.io.atom_files_directory = ['.', '../atom_files']

		a = automodel(env,
		              alnfile  = '00040-4lxj.pir',     # alignment filename
		              knowns   = '4LXJ',               # codes of the templates
		              sequence = '00040')              # code of the target
		a.starting_model= 1                 # index of the first model
		a.ending_model  = 10                # index of the last model
		                                    # (determines how many models to calculate)
		a.make()                            # do the actual comparative modeling

Note, that:

  - 4LXJ.pdb file must exist in the currend directory. 
  - 00040-4lxj.pir is the name of the alignment file. 
  - `knowns` variable definesd the temaplte PDB codes

Running Modeller
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Once the three files are ready, execute the following command:

    .. code-block:: bash
        
      mod9.22 my_modeller_script.py

It assumes you have Modeller version 9.22 installed.
