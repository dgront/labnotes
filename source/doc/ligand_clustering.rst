.. _ligand_clustering:

Clustering poses from ligand docking
================================================================

This protocol uses `ap_ligand_clustering` program of the `BioShell 3.0`_ package, which should be called as:

.. code-block:: bash
  
  ap_ligand_clustering LIG list_of_files.txt  min_cluster_size clustering_cutoff 


e.g.

.. code-block:: bash
  
  ap_ligand_clustering LIG list_of_files.txt  10 3.0 


where `LIG` is the three-letter ligand PDB code and `list_of_files.txt` file contains names of output `.pdb` files, for example:
    
.. code-block:: bash

  ./out/protein_HEM_LIG.pdb_0.pdb
  ./out/protein_HEM_LIG.pdb_1.pdb
  ./out/protein_HEM_LIG.pdb_2.pdb
  ./out/protein_HEM_LIG.pdb_3.pdb
  ./out/protein_HEM_LIG.pdb_4.pdb

The two other parameters: ``min_cluster_size`` and  ``clustering_cutoff`` control what clusters are created, i.e. their minimal size and distace cutoff, respectively. In the above example clusters of 10 or more structures will be written. Every cluster contains only these structures where their mutual distance is no longer than 3.0A.

For further information see also:  https://bioshell.readthedocs.io/en/latest/examples/ap_ligand_clustering.html

.. rubric::
  References

.. target-notes::

.. _`BioShell 3.0` : https://bioshell.readthedocs.io/en/latest/doc/installation.html
