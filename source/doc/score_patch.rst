.. _score_patch:

Score patching
==============

Rosetta uses a combination of score terms to calculate energy value for a given pose. Rosetta distributions provides several standrdised and tested sets of scoring terms and respective weights. The standard weights may be changed with a patching mechanism. an example is given below 

Using rms as a Rosetta score
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This example shows how to add *crmsd* additional score term to Rosetta energy function. The procedure  is quite easy, just follow the steps

1. preape a **patch file**

  A patch file contains the **name of a score term** and its weight, e.g.

  .. code-block:: console

    rms_energy = 1.0
  

2. Turn on the patch in the appropriate stages of *ab-initio* protocol by adding the following lines to a flag file that is used to ab-initio protocol:

  .. code-block:: console

    -abinitio::stage2_patch  crmsd_patch 
    -abinitio::stage3a_patch crmsd_patch 
    -abinitio::stage3b_patch crmsd_patch 
    -abinitio::stage4_patch  crmsd_patch
      
  where `crmsd_patch` is the name of the patch file

3. Make sure the ab-initio flag file you are using provides the reference native structure with the flag ``-in::file::native`` 
